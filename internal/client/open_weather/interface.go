package open_weather

import "agent_app_test/pkg/logging"

var weatherDataTypes = []string{
	"current",
	"minutely",
	"hourly",
	"daily",
	"alerts",
}

type OpenWeatherClient struct {
	logger *logging.Logger
	Key    string
}

func NewOpenWeatherClient(logger *logging.Logger, key string) OpenWeatherClient {
	return OpenWeatherClient{
		logger: logger,
		Key:    key,
	}
}
