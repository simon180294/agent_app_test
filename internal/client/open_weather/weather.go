package open_weather

import (
	"agent_app_test/internal/entity"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/kirinlabs/HttpRequest"
	"net/http"
	"strconv"
	"strings"
)

func (c *OpenWeatherClient) GetWeatherData(geolocation entity.CityData, dataType, lang string) (
	data entity.TotalWeatherData, err error,
) {
	excludeParam, err := prepareExcludedParams(dataType)
	if err != nil {
		c.logger.Error(err)
		return entity.TotalWeatherData{}, err
	}

	req := HttpRequest.NewRequest()
	req.SetTimeout(5)
	req.SetHeaders(map[string]string{
		"Content-Type": "application/json",
	})

	URL := "https://api.openweathermap.org/data/3.0/onecall"
	URL += "?lat=" + strconv.FormatFloat(geolocation.Lat, 'f', -1, 64)
	URL += "&lon=" + strconv.FormatFloat(geolocation.Lon, 'f', -1, 64)
	URL += "&exclude=" + excludeParam
	URL += "&appid=" + c.Key
	URL += "&lang=" + lang
	URL += "&units=metric"

	res, err := req.Get(URL)
	if err != nil {
		c.logger.Error(err)
		return entity.TotalWeatherData{}, err
	}

	if res.StatusCode() != http.StatusOK {
		err = fmt.Errorf("не удалось получить актуальные данные погоды: STATUS=%s, CITY_NAME=%s",
			res.Response().Status, geolocation.Name)
		c.logger.Error(err)
		return entity.TotalWeatherData{}, err
	}

	responseBody, err := res.Body()
	if err != nil {
		c.logger.Error(err)
		return entity.TotalWeatherData{}, err
	}

	err = json.Unmarshal(responseBody, &data)
	if err != nil {
		c.logger.Error(err)
		return entity.TotalWeatherData{}, err
	}

	return data, nil
}

// prepareExcludedParams - готовит значение query параметра exclude запроса на получение данных погоды,
// в соответствии со списком допустимых значений, для получения только необходимых данных
func prepareExcludedParams(expectedType string) (param string, err error) {
	resultCap := len(weatherDataTypes) - 1
	excludedTypes := make([]string, 0, resultCap)

	for _, dataType := range weatherDataTypes {
		if dataType != expectedType {
			if len(excludedTypes) == resultCap {
				excludedTypes = nil
				return "", errors.New("неизвестный тип запрашиваемых данных о погоде: TYPE=" + expectedType)
			}

			excludedTypes = append(excludedTypes, dataType)
		}
	}

	param = strings.Join(excludedTypes, ",")

	return param, nil
}
