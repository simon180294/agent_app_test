package open_weather

import (
	"agent_app_test/internal/entity"
	"encoding/json"
	"fmt"
	"github.com/kirinlabs/HttpRequest"
	"net/http"
)

func (c *OpenWeatherClient) GetGeolocation(cityName string) (data entity.CityData, err error) {
	req := HttpRequest.NewRequest()
	req.SetTimeout(5)
	req.SetHeaders(map[string]string{
		"Content-Type": "application/json",
	})

	URL := "http://api.openweathermap.org/geo/1.0/direct"
	URL += "?q=" + cityName
	URL += "&limit=1"
	URL += "&appid=" + c.Key

	res, err := req.Get(URL)
	if err != nil {
		c.logger.Error(err)
		return entity.CityData{}, err
	}

	if res.StatusCode() != http.StatusOK {
		err = fmt.Errorf("не удалось получить данные геопозиции по наименованию города: STATUS=%s, CITY_NAME=%s",
			res.Response().Status, cityName)
		c.logger.Error(err)
		return entity.CityData{}, err
	}

	responseBody, err := res.Body()
	if err != nil {
		c.logger.Error(err)
		return entity.CityData{}, err
	}

	var responseData []entity.CityData
	err = json.Unmarshal(responseBody, &responseData)
	if err != nil {
		c.logger.Error(err)
		return entity.CityData{}, err
	}

	if len(responseData) == 0 {
		err = fmt.Errorf("не удалось найти данные по наименованию города: CITY_NAME=%s", cityName)
		c.logger.Error(err)
		return entity.CityData{}, err
	}

	data = responseData[0]

	c.logger.Infof("Успешно получены данные геолокации: CITY=%s, LAT=%f, LON=%f", data.Name, data.Lat, data.Lon)

	return data, nil
}
