package middleware

import (
	"agent_app_test/internal/config"
	"errors"
	"net/http"
)

var debug = config.GetConfig().Debug

type AppHandler func(w http.ResponseWriter, r *http.Request) error

func (f AppHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	err := f(w, r)
	if err != nil {
		return
	}
}

type Middleware func(h AppHandler) AppHandler
type Chain []Middleware

func New(middlewares ...Middleware) Chain {
	var slice Chain
	return append(slice, middlewares...)
}

func (c Chain) Then(handler AppHandler) AppHandler {
	for i := range c {
		handler = c[len(c)-1-i](handler)
	}
	return handler
}

func ErrorMiddleware(h AppHandler) AppHandler {
	return func(w http.ResponseWriter, r *http.Request) error {
		var appErr *AppErr

		w.Header().Set("Content-Type", "application/json")

		err := h(w, r)

		if err != nil {
			if errors.As(err, &appErr) {
				if errors.Is(err, NotFound) {
					w.WriteHeader(http.StatusNotFound)
					_, _ = w.Write(NotFound.Marshal())

					return nil

				} else if errors.Is(err, BadRequest) {
					w.WriteHeader(http.StatusBadRequest)
					_, _ = w.Write(BadRequest.Marshal())

					return nil

				} else if errors.Is(err, Unauthorized) {
					w.WriteHeader(http.StatusUnauthorized)
					_, _ = w.Write(Unauthorized.Marshal())

					return nil

				} else if errors.Is(err, RequestTimeout) {
					w.WriteHeader(http.StatusRequestTimeout)
					_, _ = w.Write(Unauthorized.Marshal())

					return nil

				} else if errors.Is(err, Forbidden) {
					w.WriteHeader(http.StatusForbidden)
					_, _ = w.Write(Forbidden.Marshal())

					return nil

				} else if appErr.Code == "DS-200002" {
					w.WriteHeader(http.StatusAccepted)
					_, _ = w.Write(appErr.Marshal())

					return nil

				} else if appErr.Code == "DS-500010" {
					w.WriteHeader(http.StatusNotExtended)
					_, _ = w.Write(appErr.Marshal())

					return nil
				}

				w.WriteHeader(http.StatusBadRequest)
				_, _ = w.Write(BadRequest.Marshal())

				return nil

			}

			w.WriteHeader(http.StatusBadRequest)
			if debug {
				_, _ = w.Write(systemError(err).Marshal())

			} else {
				_, _ = w.Write(BadRequest.Marshal())
			}
		}
		return nil
	}
}
