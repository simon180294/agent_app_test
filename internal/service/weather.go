package service

import (
	"agent_app_test/internal/entity"
	"context"
	"errors"
	"time"
)

func (s *Service) WeatherData(cityName, dataType, lang string) (data entity.TotalWeatherData, err error) {
	// получение данных геолокации
	var geolocation entity.CityData
	geolocation, err = s.owClient.GetGeolocation(cityName)
	if err != nil {
		return entity.TotalWeatherData{}, err
	}

	cacheKey := geolocation.GetCacheKey(dataType)

	// проверка наличия запрашиваемых данных в хранилище Redis
	existsCtx, existsCancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer existsCancel()

	existsInRedis := s.redis.Exists(existsCtx, cacheKey)

	if existsInRedis { // попытка получения данные погоды из хранилища Redis
		getCtx, getCancel := context.WithTimeout(context.Background(), 2*time.Second)
		defer getCancel()

		err = s.redis.Get(getCtx, cacheKey, &data)
		// истечение времени ожидания ответа от Redis - попытка получения данных от клиента
		if errors.Is(err, context.DeadlineExceeded) {
			s.logger.Warningf("истечение времени ожидания получения данных о погоде из Redis: KEY=%s", cacheKey)
			goto getDataFromClient

		} else if err != nil { // прочие ошибки
			s.logger.Errorf(
				"не удалось получить данные о погоде из Redis: KEY=%s, ERROR=%s", cacheKey, err.Error())
			return entity.TotalWeatherData{}, err

		} else { // данные успешно получены из Redis
			s.logger.Infof("Успешно получены данные из Redis: KEY=%s", cacheKey)

			return data, nil
		}
	}

	// получение данных погоды от клиента
getDataFromClient:
	data, err = s.owClient.GetWeatherData(geolocation, dataType, lang)
	if err != nil {
		return entity.TotalWeatherData{}, err
	}

	s.logger.Infof("Успешно получены данные от клиента: KEY=%s", cacheKey)

	// создание данных в хранилище Redis для дальнейшего использования
	setCtx, setCancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer setCancel()

	err = s.asyncSaveToRedis(setCtx, cacheKey, data)
	if err != nil {
		s.logger.Errorf("не удалось сохранить данные в Redis: KEY=%s, ERROR=%s", cacheKey, err.Error())

	} else {
		s.logger.Infof("Успешно созданы данные в Redis: KEY=%s", cacheKey)
	}

	return data, nil
}
