package service

import (
	"context"
	"github.com/go-redis/cache/v9"
	"time"
)

func (s *Service) asyncSaveToRedis(ctx context.Context, key string, data any) (err error) {
	errCh := make(chan error, 1)

	go func() {
		err = s.redis.Set(&cache.Item{
			Ctx:   ctx,
			Key:   key,
			Value: data,
			TTL:   time.Minute * 30,
		})
		errCh <- err
	}()

	select {
	case <-ctx.Done():
		err = ctx.Err()
		return err

	case err = <-errCh:
		return err
	}
}
