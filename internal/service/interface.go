package service

import (
	"agent_app_test/internal/entity"
	"agent_app_test/pkg/logging"
	"github.com/go-redis/cache/v9"
)

type OpenWeather interface {
	GetGeolocation(cityName string) (data entity.CityData, err error)
	GetWeatherData(geolocation entity.CityData, dataType, lang string) (data entity.TotalWeatherData, err error)
}

type Service struct {
	logger   *logging.Logger
	redis    *cache.Cache // Клиент хранилища Redis
	owClient OpenWeather  // Клиент сервиса Open Weather
}

func NewService(logger *logging.Logger, redis *cache.Cache, ow OpenWeather) Service {
	return Service{
		logger:   logger,
		redis:    redis,
		owClient: ow,
	}
}
