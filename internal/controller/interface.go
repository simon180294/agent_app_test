package controller

import (
	"agent_app_test/internal/entity"
	"agent_app_test/internal/middleware"
	"agent_app_test/pkg/logging"
	"github.com/julienschmidt/httprouter"
	httpSwagger "github.com/swaggo/http-swagger"
	"net/http"
)

const (
	currentURL = "/api/v1/agent-app-weather/current/"
	hourlyURL  = "/api/v1/agent-app-weather/hourly/"
	dailyURL   = "/api/v1/agent-app-weather/daily/"
	swaggerURL = "/api/v1/agent-app-weather/swagger/*any"
)

type service interface {
	WeatherData(cityName, dataType, lang string) (data entity.TotalWeatherData, err error)
}

type Controller struct {
	logger  *logging.Logger
	service service
}

func NewController(logger *logging.Logger, service service) Controller {
	return Controller{
		logger:  logger,
		service: service,
	}
}

func (h *Controller) Register(router *httprouter.Router) {
	router.Handler(http.MethodGet, currentURL, middleware.New(middleware.ErrorMiddleware).Then(h.CurrentWeather))
	router.Handler(http.MethodGet, hourlyURL, middleware.New(middleware.ErrorMiddleware).Then(h.HourlyWeather))
	router.Handler(http.MethodGet, dailyURL, middleware.New(middleware.ErrorMiddleware).Then(h.DailyWeather))

	router.Handler(http.MethodGet, swaggerURL, httpSwagger.WrapHandler)
}
