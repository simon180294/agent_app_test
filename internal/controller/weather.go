package controller

import (
	"agent_app_test/internal/entity"
	"encoding/json"
	"errors"
	"net/http"
)

// CurrentWeather
// @Summary
// @Tags Weather
// @Description Получение данных погоды на текущий момент.
// @Accept  json
// @Produce  json
// @param Accept-Language header string true "Accept-Language" Enums(ru, en, kz)
// @Param city_name query string true "Наименование города"
// @Success 200 {object} entity.CurrentWeather
// @Failure 400,404 {object} error
// @Failure 500 {object} error
// @Failure default {object} error
// @Router /current/ [get]
func (h *Controller) CurrentWeather(w http.ResponseWriter, r *http.Request) (err error) {
	cityName := r.URL.Query().Get("city_name")
	if cityName == "" {
		err = errors.New("отсутствует обязательный параметр - наименование города")
		h.logger.Error(err)
		return err
	}

	lang := r.Header.Get("Accept-Language")
	if lang == "" {
		lang = "ru"
	}

	var weatherData entity.TotalWeatherData
	weatherData, err = h.service.WeatherData(cityName, "current", lang)
	if err != nil {
		return err
	}

	var bytes []byte
	bytes, err = json.Marshal(weatherData.Current)
	if err != nil {
		h.logger.Error(err)
		return err
	}

	_, err = w.Write(bytes)
	if err != nil {
		h.logger.Error(err)
		return err
	}

	w.WriteHeader(http.StatusOK)

	return nil
}

// HourlyWeather
// @Summary
// @Tags Weather
// @Description Получение почасовых данных погоды.
// @Accept  json
// @Produce  json
// @param Accept-Language header string true "Accept-Language" Enums(ru, en, kz)
// @Param city_name query string true "Наименование города"
// @Success 200 {object} []entity.HourlyWeather
// @Failure 400,404 {object} error
// @Failure 500 {object} error
// @Failure default {object} error
// @Router /hourly/ [get]
func (h *Controller) HourlyWeather(w http.ResponseWriter, r *http.Request) (err error) {
	cityName := r.URL.Query().Get("city_name")
	if cityName == "" {
		err = errors.New("отсутствует обязательный параметр - наименование города")
		h.logger.Error(err)
		return err
	}

	lang := r.Header.Get("Accept-Language")
	if lang == "" {
		lang = "ru"
	}

	var weatherData entity.TotalWeatherData
	weatherData, err = h.service.WeatherData(cityName, "hourly", lang)
	if err != nil {
		return err
	}

	var bytes []byte
	bytes, err = json.Marshal(weatherData.Hourly)
	if err != nil {
		h.logger.Error(err)
		return err
	}

	_, err = w.Write(bytes)
	if err != nil {
		h.logger.Error(err)
		return err
	}

	w.WriteHeader(http.StatusOK)

	return nil
}

// DailyWeather
// @Summary
// @Tags Weather
// @Description Получение почасовых данных погоды.
// @Accept  json
// @Produce  json
// @param Accept-Language header string true "Accept-Language" Enums(ru, en, kz)
// @Param city_name query string true "Наименование города"
// @Success 200 {object} []entity.HourlyWeather
// @Failure 400,404 {object} error
// @Failure 500 {object} error
// @Failure default {object} error
// @Router /daily/ [get]
func (h *Controller) DailyWeather(w http.ResponseWriter, r *http.Request) (err error) {
	cityName := r.URL.Query().Get("city_name")
	if cityName == "" {
		err = errors.New("отсутствует обязательный параметр - наименование города")
		h.logger.Error(err)
		return err
	}

	lang := r.Header.Get("Accept-Language")
	if lang == "" {
		lang = "ru"
	}

	var weatherData entity.TotalWeatherData
	weatherData, err = h.service.WeatherData(cityName, "daily", lang)
	if err != nil {
		return err
	}

	var bytes []byte
	bytes, err = json.Marshal(weatherData.Daily)
	if err != nil {
		h.logger.Error(err)
		return err
	}

	_, err = w.Write(bytes)
	if err != nil {
		h.logger.Error(err)
		return err
	}

	w.WriteHeader(http.StatusOK)

	return nil
}
