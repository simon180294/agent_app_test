package config

import (
	"agent_app_test/pkg/logging"
	"github.com/ilyakaznacheev/cleanenv"
	"sync"
)

type Config struct {
	Debug       bool        `yaml:"debug"`
	Listen      listen      `yaml:"listen"`
	Redis       RedisConfig `yaml:"redis"`
	Cors        Cors        `yaml:"cors"`
	OpenWeather OpenWeather `yaml:"open_weather"`
}

type listen struct {
	Host string `yaml:"bind_ip" env-default:"127.0.0.1"`
	Port string `yaml:"port" env-default:"3050"`
}

type RedisConfig struct {
	Host     string `json:"host"`
	Port     string `json:"port"`
	Username string `json:"username"`
	Password string `json:"password"`
}

type Cors struct {
	AllowedOrigins     []string `json:"allowed_origins" yaml:"allowed_origins"`
	AllowedMethods     []string `json:"allowed_methods" yaml:"allowed_methods"`
	AllowedHeaders     []string `json:"allowed_headers" yaml:"allowed_headers"`
	ExposedHeaders     []string `json:"exposed_headers" yaml:"exposed_headers"`
	AllowCredentials   bool     `json:"allow_credentials" yaml:"allow_credentials"`
	OptionsPassthrough bool     `json:"options_passthrough" yaml:"options_passthrough"`
	Debug              bool     `json:"debug" yaml:"debug"`
}

type OpenWeather struct {
	Key string `yaml:"key"`
}

var instance *Config
var once sync.Once

func GetConfig() *Config {
	once.Do(func() {
		logger := logging.GetLogger()
		logger.Info("read application configurations")
		instance = &Config{}

		if err := cleanenv.ReadConfig("config.yml", instance); err != nil {
			help, _ := cleanenv.GetDescription(instance, nil)
			logger.Info(help)
			logger.Fatal(err)
		}
	})
	return instance
}
