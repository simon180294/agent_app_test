package entity

type TotalWeatherData struct {
	TimezoneOffset int             `json:"timezone_offset"` // Сдвиг в секундах от UTC
	Lat            float64         `json:"lat"`             // Широта местоположения, десятичная
	Lon            float64         `json:"lon"`             // Долгота местоположения, десятичная
	Timezone       string          `json:"timezone"`        // Название часового пояса для запрошенного местоположения
	Hourly         []HourlyWeather `json:"hourly"`          // Данные о погоде с почасовым прогнозом
	Daily          []DailyWeather  `json:"daily"`           // Ежедневный прогноз погоды
	Current        CurrentWeather  `json:"current"`         // Текущие данные о погоде
}

type WeatherDescription struct {
	Description string `json:"description"` // Описание погодных условий
	Icon        string `json:"icon"`        // Идентификатор значка погоды
}

type CurrentWeather struct {
	WeatherData
	Sunrise int `json:"sunrise"` // Время восхода солнца, Unix, UTC
	Sunset  int `json:"sunset"`  // Время захода солнца, Unix, UTC
}

type HourlyWeather struct {
	WeatherData
	WindGust float64  `json:"wind_gust"`      // Порыв ветра. Единицы измерения зависят от запроса
	Pop      float64  `json:"pop"`            // Вероятность выпадения осадков. От 0 до 1, где 0 равно 0%, 1 равно 100%
	Rain     Rainfall `json:"rain,omitempty"` // Данные о дожде
	Snow     Rainfall `json:"snow,omitempty"` // Данные о выпадении снега
}

type Rainfall struct {
	OneH float64 `json:"1h"` // Количество осадков, мм / ч.
}

type WeatherData struct {
	Dt         int                  `json:"dt"`         // Текущее время, Unix, UTC
	Pressure   int                  `json:"pressure"`   // Атмосферное давление на уровне моря, ГПа
	Humidity   int                  `json:"humidity"`   // Влажность, %
	Clouds     int                  `json:"clouds"`     // Облачность, %
	Visibility int                  `json:"visibility"` // Средняя видимость, м. Максимальное значение видимости составляет 10 км
	WindDeg    int                  `json:"wind_deg"`   // Направление ветра, градусы
	Temp       float64              `json:"temp"`       // Температура. Единицы измерения зависят от запроса
	FeelsLike  float64              `json:"feels_like"` // Температура. Этот параметр температуры учитывает восприятие погоды человеком
	DewPoint   float64              `json:"dew_point"`  // Температура атмосферы, ниже которой начинают конденсироваться капли воды и может образовываться роса.
	Uvi        float64              `json:"uvi"`        // Текущий УФ-индекс
	WindSpeed  float64              `json:"wind_speed"` // Скорость ветра. Единицы измерения зависят от запроса
	Weather    []WeatherDescription `json:"weather"`    // Описание погодных условий
}

type DailyWeather struct {
	Dt        int     `json:"dt"`         // Время получения прогнозируемых данных, Unix, UTC
	Sunrise   int     `json:"sunrise"`    // Время восхода солнца, Unix, UTC
	Sunset    int     `json:"sunset"`     // Время захода солнца, Unix, UTC
	Moonrise  int     `json:"moonrise"`   // Время восхода луны в этот день, Unix, UTC
	Moonset   int     `json:"moonset"`    // Время захода луны в этот день, Unix, UTC
	MoonPhase float64 `json:"moon_phase"` // Фаза Луны. 0 и 1 - это "новолуние", 0.25 - "первая четверть луны", 0.5 - "полнолуние" и 0.75 - "последняя четверть луны"
	Summary   string  `json:"summary"`    // Описание погодных условий
	Temp      struct {
		Day   float64 `json:"day"`   // Дневная температура
		Min   float64 `json:"min"`   // Минимальная суточная температура
		Max   float64 `json:"max"`   // Максимальная суточная температура
		Night float64 `json:"night"` // Ночная температура
		Eve   float64 `json:"eve"`   // Вечерняя температура
		Morn  float64 `json:"morn"`  // Утренняя температура
	} `json:"temp"` // Температура. Единицы измерения зависят от запроса
	FeelsLike struct {
		Day   float64 `json:"day"`   // Дневная температура
		Night float64 `json:"night"` // Ночная температура
		Eve   float64 `json:"eve"`   // Вечерняя температура
		Morn  float64 `json:"morn"`  // Утренняя температура
	} `json:"feels_like"` // Температура. Этот параметр температуры учитывает восприятие погоды человеком
	Pressure  int                  `json:"pressure"`       // Атмосферное давление на уровне моря, ГПа
	Humidity  int                  `json:"humidity"`       // Влажность, %
	WindDeg   int                  `json:"wind_deg"`       // Направление ветра, градусы
	Clouds    int                  `json:"clouds"`         // Облачность, %
	Pop       float64              `json:"pop"`            // Вероятность выпадения осадков. От 0 до 1, где 0 равно 0%, 1 равно 100%
	DewPoint  float64              `json:"dew_point"`      // Температура атмосферы, ниже которой начинают конденсироваться капли воды и может образовываться роса.
	WindSpeed float64              `json:"wind_speed"`     // Скорость ветра. Единицы измерения зависят от запроса
	WindGust  float64              `json:"wind_gust"`      // Порыв ветра. Единицы измерения зависят от запроса
	Uvi       float64              `json:"uvi"`            // УФ-индекс
	Rain      float64              `json:"rain,omitempty"` //  Объем осадков, мм
	Snow      float64              `json:"snow,omitempty"` //  Объем снега, мм
	Weather   []WeatherDescription `json:"weather"`        // Описание погодных условий
}
