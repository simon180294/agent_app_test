package entity

import "strconv"

type CityData struct {
	Name    string  `json:"name"`
	Lat     float64 `json:"lat"`
	Lon     float64 `json:"lon"`
	Country string  `json:"country"`
}

// GetCacheKey - формирует ключ для получения\создания данных в хранилище Redis:
// {тип погодных данных}_{широта}_{долгота}
func (d *CityData) GetCacheKey(dataType string) string {
	return dataType + "_" +
		strconv.FormatFloat(d.Lat, 'f', -1, 64) + "_" +
		strconv.FormatFloat(d.Lon, 'f', -1, 64)
}
