package main

import (
	_ "agent_app_test/docs"
	"agent_app_test/internal/client/open_weather"
	"agent_app_test/internal/config"
	"agent_app_test/internal/controller"
	"agent_app_test/internal/service"
	"agent_app_test/pkg/cors"
	"agent_app_test/pkg/logging"
	"context"
	"fmt"
	"github.com/go-redis/cache/v9"
	"github.com/julienschmidt/httprouter"
	"github.com/redis/go-redis/v9"
	"net"
	"net/http"
	"time"
)

// @title Agent App Weather API
// @version 1.0
// @description API Для получения данных о погоде

// @host 0.0.0.0:3050
// @BasePath /api/v1/agent-app-weather
func main() {
	cfg := config.GetConfig()
	router := httprouter.New()
	logger := logging.GetLogger()

	redisCache, err := redisConnect(cfg, logger)
	if err != nil {
		logger.Fatalf("Redis connection was refused: %v", err)
	}

	openWeatherClient := open_weather.NewOpenWeatherClient(logger, cfg.OpenWeather.Key)

	newService := service.NewService(logger, redisCache, &openWeatherClient)

	restController := controller.NewController(logger, &newService)
	restController.Register(router)

	runRest(router, cfg)
}

func runRest(router *httprouter.Router, cfg *config.Config) {
	logger := logging.GetLogger()

	var listener net.Listener
	var listenErr error

	listener, listenErr = net.Listen("tcp", fmt.Sprintf("%s:%s", cfg.Listen.Host, cfg.Listen.Port))
	logger.Infof("server is listening %s:%s", cfg.Listen.Host, cfg.Listen.Port)

	if listenErr != nil {
		panic(listenErr)
	}

	corsSettings := cors.GetCorsSettings(cfg)

	header := corsSettings.Handler(router)

	server := &http.Server{
		Handler:      header,
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	logger.Fatal(server.Serve(listener))
}

func redisConnect(cfg *config.Config, logger *logging.Logger) (*cache.Cache, error) {
	ring := redis.NewRing(&redis.RingOptions{
		Addrs: map[string]string{
			"server": fmt.Sprintf("%s:%s", cfg.Redis.Host, cfg.Redis.Port),
		},
	})

	_, err := ring.Ping(context.Background()).Result()
	if err != nil {
		return nil, err
	}

	redisCache := cache.New(&cache.Options{
		Redis:      ring,
		LocalCache: cache.NewTinyLFU(1000, time.Minute),
	})

	logger.Infof("connected to Redis %s:%s", cfg.Redis.Host, cfg.Redis.Port)

	return redisCache, nil
}
